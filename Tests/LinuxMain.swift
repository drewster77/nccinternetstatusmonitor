import XCTest

import NCCInternetStatusMonitorTests

var tests = [XCTestCaseEntry]()
tests += NCCInternetStatusMonitorTests.allTests()
XCTMain(tests)
