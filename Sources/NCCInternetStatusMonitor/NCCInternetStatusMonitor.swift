//
//  NCCInternetStatusMonitor.swift
//  NCCInternetStatusMonitor
//
//  Created by Andrew Benson on 3/15/20.
//  Copyright © 2020 Nuclear Cyborg Corp. All rights reserved.
//

import Foundation
import Network

@available(macOS 10.15, iOS 13.0, *)
public class NCCInternetStatusMonitor: ObservableObject {

    public static var shared: NCCInternetStatusMonitor = NCCInternetStatusMonitor()
    
    /// True if our network paths currently have Internet access
    @Published private(set) public var isInternetConnected = false {
        didSet {
            delegate?.nccInternetStatusMonitor(self, connectivityDidChange: isInternetConnected)
        }
    }

    /// Delegate to receive status updates.  Updates are returned on the main queue.
    public weak var delegate: NCCInternetStatusMonitorDelegate?

    /// Apple's handy-dandy monitoring thingy
    private var pathMonitor: NWPathMonitor

    /// Init
    public init() {
        pathMonitor = NWPathMonitor()
        pathMonitor.pathUpdateHandler = updateHandler
        pathMonitor.start(queue: DispatchQueue.main)
    }

    /// Handles updates delivered by NWPathMonitor
    /// - Parameter path: The updated NWPath
    private func updateHandler(_ path: NWPath) {
        print("Received path update: \(path) with status \(path.status)")
        switch path.status {
        case .requiresConnection:
            isInternetConnected = false

        case .satisfied:
            isInternetConnected = true

        case .unsatisfied:
            isInternetConnected = false

        @unknown default:
            print("\(String(describing: type(of: self))): \(#function): path status \(path.status) is unknown.  Update this module.")
            #if DEBUG
            fatalError("\(String(describing: type(of: self))): \(#function): path status \(path.status) is unknown.  Update this module.")
            #endif
        }
    }
}

/// Delegate protocol interface for receiving internet connectivity status updates.
@available(macOS 10.15, iOS 13.0, *)
public protocol NCCInternetStatusMonitorDelegate: class {
    func nccInternetStatusMonitor(_ nccInternetStatusMonitor: NCCInternetStatusMonitor, connectivityDidChange isConnected: Bool)
}
